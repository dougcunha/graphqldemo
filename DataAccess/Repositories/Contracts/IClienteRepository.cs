﻿namespace GraphQLDemo.DataAccess.Repositories.Contracts
{
    using System.Collections.Generic;
    using GraphQLDemo.DataBase.Models;

    public interface IClienteRepository
    {
        IEnumerable<Cliente> GetAll();
    }
}