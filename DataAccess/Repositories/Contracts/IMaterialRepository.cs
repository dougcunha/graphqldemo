﻿namespace GraphQLDemo.DataAccess.Repositories.Contracts
{
    #region usings
    using System.Collections.Generic;
    using GraphQLDemo.DataBase.Models;
    #endregion

    public interface IMaterialRepository
    {
        IEnumerable<Material> GetAll();
        IEnumerable<Material> GetAll(int first);

        Material Get(int id);

        Material Get(string codigo);
        IEnumerable<Material> GetLast(int last);
        Material Add(Material material);
        Material Update(Material material);
        bool Delete(int id);
        bool Delete(string codigo);
    }
}