﻿namespace GraphQLDemo.DataAccess.Repositories
{
    #region usings
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataBase;
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.DataBase.Models;
    #endregion

    public class MaterialRepository : IMaterialRepository
    {
        private readonly CatalogoContexto _dbContext;

        public MaterialRepository(CatalogoContexto dbContext)
            => _dbContext = dbContext;

        #region Implementation of IMaterialRepository
        /// <inheritdoc />
        public IEnumerable<Material> GetAll()
            => _dbContext.Material;

        /// <inheritdoc />
        public IEnumerable<Material> GetAll(int first)
            => _dbContext.Material.OrderBy(m => m.Id).Take(first);

        /// <inheritdoc />
        public Material Get(int id)
            => _dbContext.Material.SingleOrDefault(m => m.Id == id);

        /// <inheritdoc />
        public Material Get(string codigo)
            => _dbContext.Material.SingleOrDefault(m => m.Codigo == codigo);

        /// <inheritdoc />
        public IEnumerable<Material> GetLast(int last)
            => _dbContext.Material.OrderByDescending(m => m.Id).Take(last);

        /// <inheritdoc />
        public Material Add(Material material)
        {
            material.DtAlt = DateTime.Now;
            _dbContext.Material.Add(material);
            _dbContext.SaveChanges();

            return material;
        }

        /// <inheritdoc />
        public Material Update(Material material)
        {
            material.DtAlt = DateTime.Now;
            var materialDb = Get(material.Codigo);

            materialDb.Descricao = material.Descricao;

            _dbContext.SaveChanges();

            return material;
        }

        /// <inheritdoc />
        public bool Delete(int id)
        {
            var material = Get(id);
            _dbContext.Remove(material);
            _dbContext.SaveChanges();

            return true;
        }

        /// <inheritdoc />
        public bool Delete(string codigo)
        {
            var material = Get(codigo);
            _dbContext.Remove(material);
            _dbContext.SaveChanges();

            return true;
        }
        #endregion
    }
}