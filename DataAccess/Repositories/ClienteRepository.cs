﻿namespace GraphQLDemo.DataAccess.Repositories
{
    #region usings
    using System.Collections.Generic;
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.DataBase;
    using GraphQLDemo.DataBase.Models;
    #endregion

    public class ClienteRepository : IClienteRepository
    {
        private readonly CatalogoContexto _dbContext;

        public ClienteRepository(CatalogoContexto dbContext)
            => _dbContext = dbContext;

        public IEnumerable<Cliente> GetAll()
            => _dbContext.Cliente;
    }
}