﻿namespace GraphQLDemo.Schema
{
    #region usings
    using GraphQL;
    using GraphQL.Types;
    using GraphQLDemo.Queries;
    #endregion

    public class CatalogoSchema : Schema
    {
        public CatalogoSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<CatalogoQuery>();
            Mutation = resolver.Resolve<CatalogoMutation>();
        }
    }
}