﻿namespace GraphQLDemo.Queries
{
    #region usings
    using GraphQL.Types;
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.DataBase.Models;
    using GraphQLDemo.Types;
    #endregion

    public class CatalogoMutation : ObjectGraphType
    {
        private readonly IMaterialRepository _materialRepository;

        public CatalogoMutation(IMaterialRepository materialRepository)
        {
            _materialRepository = materialRepository;

            MaterialMutations();
        }

        private void MaterialMutations()
        {
            Field<MaterialType>
            (
                "novoMaterial",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<MaterialInputType>> { Name = "material" }),
                resolve: ctx =>
                {
                    var material = ctx.GetArgument<Material>("material");

                    return _materialRepository.Add(material);
                }
            );

            Field<MaterialType>
            (
                "atualizarMaterial",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<MaterialInputType>> { Name = "material" }),
                resolve: ctx =>
                {
                    var material = ctx.GetArgument<Material>("material");
                    
                    return _materialRepository.Update(material);
                }
            );
            
            Field<BooleanGraphType>
            (
                "apagarMaterial",
                arguments: new QueryArguments
                (
                    new QueryArgument<IntGraphType> { Name = "ID" },
                    new QueryArgument<StringGraphType> { Name = "Codigo" }
                ),
                resolve: ctx =>
                {
                    var id = ctx.GetArgument<int?>("ID");
                
                    if (id != null)
                        return _materialRepository.Delete(id.Value);

                    var codigo = ctx.GetArgument<string>("codigo");

                    return codigo != null && _materialRepository.Delete(codigo);
                }
            );
        }
    }
}