﻿namespace GraphQLDemo.Queries
{
    #region usings
    using GraphQL.Types;
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.Types;
    #endregion

    public class CatalogoQuery : ObjectGraphType
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly IMaterialRepository _materialRepository;

        public CatalogoQuery(IMaterialRepository materialRepository, IClienteRepository clienteRepository)
        {
            _materialRepository = materialRepository;
            _clienteRepository = clienteRepository;

            MaterialQuery();
            ClienteQuery();
        }

        private void ClienteQuery()
        {
            Field<ListGraphType<ClienteType>>
            (
                "clientes",
                resolve: ctx => _clienteRepository.GetAll()
            );
        }

        private void MaterialQuery()
        {
            Field<ListGraphType<MaterialType>>
            (
                "materiais",
                arguments: new QueryArguments
                (
                    new QueryArgument<IntGraphType>
                    {
                        Name = "first", 
                        Description = "Quantidade máxima de materiais a partir do início."
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "last", 
                        Description = "Quantidade máxima de materiais do fim da lista."
                    }
                ),
                resolve: ctx =>
                {
                    var first = ctx.GetArgument<int?>("first");
                    var last = ctx.GetArgument<int?>("last");

                    return first != null ? _materialRepository.GetAll(first.Value) :
                        last != null ? _materialRepository.GetLast(last.Value) : 
                        _materialRepository.GetAll();
                }
            );

            Field<MaterialType>
            (
                "material",
                arguments: new QueryArguments
                (
                    new QueryArgument<StringGraphType>
                    {
                        Name = "codigo", 
                        Description = "Código do material."
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "id", 
                        Description = "ID do material"
                    }
                ),
                resolve: ctx =>
                {
                    var id = ctx.GetArgument<int?>("id");
                    var codigo = ctx.GetArgument<string>("codigo");

                    return id != null
                        ? _materialRepository.Get(id.Value)
                        : _materialRepository.Get(codigo);
                }
            );
        }
    }
}