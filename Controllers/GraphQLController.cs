﻿namespace GraphQLDemo.Controllers
{
    using System;
    using System.Threading.Tasks;
    using GraphQL;
    using GraphQL.Instrumentation;
    using GraphQL.Types;
    using GraphQLDemo.Utilities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore.Internal;

    [Route("[controller]")]
    // ReSharper disable once InconsistentNaming
    public class GraphQLController : Controller
    {
        private readonly ISchema _schema;
        private readonly IDocumentExecuter _documentExecuter;

        public GraphQLController(ISchema schema, IDocumentExecuter documentExecuter)
        {
            _schema = schema;
            _documentExecuter = documentExecuter;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            var start = DateTime.UtcNow;

            if (query == null)
                throw new ArgumentNullException(nameof(query));

            var inputs = query.Variables?.ToInputs();

            var executionOptions = new ExecutionOptions
            {
                Schema = _schema,
                Query = query.Query,
                Inputs = inputs,
                EnableMetrics = true
            };

            var result = await _documentExecuter.ExecuteAsync(executionOptions);

            if (result.Errors?.Any() == true)
                return BadRequest(result);

            result.EnrichWithApolloTracing(start);

            return Ok(result);
        }
    }
}