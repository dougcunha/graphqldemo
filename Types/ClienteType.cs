﻿namespace GraphQLDemo.Types
{
    #region usings
    using GraphQL.Types;
    using GraphQLDemo.DataBase.Models;
    #endregion

    public class ClienteType : ObjectGraphType<Cliente>
    {
        public ClienteType()
        {
            Field(c => c.Id, type: typeof(IdGraphType));
            Field(c => c.Identificacao);
            Field(c => c.TipoId, nullable: true, type: typeof(IntGraphType));
            Field(c => c.RegiaoId, nullable: true);
            Field(c => c.MidiaId, type: typeof(IntGraphType));
            Field(c => c.Nome);
            Field(c => c.Sexo);
            Field(c => c.CnpjCpf);
            Field(c => c.IeRg);
            Field(c => c.Endereco);
            Field(c => c.Numero, nullable: true);
            Field(c => c.Complemento);
            Field(c => c.Bairro);
            Field(c => c.Referencia);
            Field(c => c.Cep);
            Field(c => c.Cidade);
            Field(c => c.Estado);
            Field(c => c.Telefone);
            Field(c => c.Email);
            Field(c => c.Fax);
            Field(c => c.DescontoId, nullable: true);
            Field(c => c.DtValidadeDesconto, nullable: true);
            Field(c => c.DtHrPrimeiraCompra, nullable: true);
            Field(c => c.DtCadastro, nullable: true);
            Field(c => c.Ativo, nullable: true);
            Field(c => c.Conta);
            Field(c => c.DtValidadeConta, nullable: true);
            Field(c => c.LimiteConta, nullable: true);
            Field(c => c.DtAlt, nullable: true);
            Field(c => c.IdExterno, nullable: true);
            Field(c => c.LoteId, nullable: true);
            Field(c => c.Status, nullable: true, type: typeof(IntGraphType));
            Field(c => c.DtNascimento, nullable: true);
            Field(c => c.DtHrUltimaCompra, nullable: true);
        }
    }
}