﻿namespace GraphQLDemo.Types
{
    #region usings
    using GraphQL.Types;
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.DataBase.Models;
    #endregion

    public class MaterialType : ObjectGraphType<Material>
    {
        public MaterialType(IClienteRepository clienteRepository)
        {
            Field(m => m.Id)
           .Description("ID do material.");

            Field(expression: m => m.Ativo, true)
           .Description("Se o material está ativo.");

            Field(m => m.DtAlt)
           .Description("Data de alteração do material.");

            Field(m => m.Codigo)
           .Description("Código do material.");

            Field(m => m.Descricao)
           .Description("Descrição do material.");

            Field(m => m.DescricaoTouch);
            Field(m => m.DescricaoProducao);
            Field(m => m.TeclaProg);
            Field(m => m.Imagem);
            Field(m => m.CodExterno);
            Field(m => m.Unidade);
            Field(m => m.Venda);
            Field(m => m.Servico);
            Field(m => m.RequerObs);
            Field(m => m.QtdeFrac);
            Field(m => m.Balanca);
            Field(m => m.Consumacao);
            Field(m => m.VendeCombo);
            Field(m => m.VendeWeb);
            Field(m => m.GrupoId);
            Field(expression: m => m.LocalProducaoId, true);
            Field(expression: m => m.LojaId, true);
            Field(m => m.RedeId);
            Field(expression: m => m.SubRedeId, true);
            Field(expression: m => m.CodigoNum, true);
            Field(m => m.DescricaoExtra);
        }
    }
}