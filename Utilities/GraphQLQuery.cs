﻿namespace GraphQLDemo.Utilities
{
    using Newtonsoft.Json.Linq;

    // ReSharper disable once InconsistentNaming
    public class GraphQLQuery
    {
        public string OperationName { get; set; }
        public string Query { get; set; }
        public JObject Variables { get; set; }
    }
}