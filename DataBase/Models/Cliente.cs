﻿namespace GraphQLDemo.DataBase.Models
{
    using System;
    using System.Collections.Generic;

    // ReSharper disable once PartialTypeWithSinglePart
    public partial class Cliente
    {
        public Cliente()
        // ReSharper disable once VirtualMemberCallInConstructor
            => Ticket = new HashSet<Ticket>();

        public Guid Id { get; set; }
        public string Identificacao { get; set; }
        public short? TipoId { get; set; }
        public int? RegiaoId { get; set; }
        public short MidiaId { get; set; }
        public string Nome { get; set; }
        public string Sexo { get; set; }
        public string CnpjCpf { get; set; }
        public string IeRg { get; set; }
        public string Endereco { get; set; }
        public int? Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Referencia { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public byte[] Foto { get; set; }
        public int? DescontoId { get; set; }
        public DateTime? DtValidadeDesconto { get; set; }
        public DateTime? DtHrPrimeiraCompra { get; set; }
        public DateTime? DtCadastro { get; set; }
        public bool? Ativo { get; set; }
        public bool Conta { get; set; }
        public DateTime? DtValidadeConta { get; set; }
        public double? LimiteConta { get; set; }
        public DateTime? DtAlt { get; set; }
        public int? IdExterno { get; set; }
        public long? LoteId { get; set; }
        public short? Status { get; set; }
        public DateTime? DtNascimento { get; set; }
        public DateTime? DtHrUltimaCompra { get; set; }

        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
