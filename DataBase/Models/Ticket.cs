﻿namespace GraphQLDemo.DataBase.Models
{
    using System;

    // ReSharper disable once PartialTypeWithSinglePart
    public partial class Ticket
    {
        public Guid TicketId { get; set; }
        public Guid? TicketPaiId { get; set; }
        public int ModoVendaId { get; set; }
        public string Estado { get; set; }
        public int Codigo { get; set; }
        public int? PracaId { get; set; }
        public bool Ativo { get; set; }
        public double? PctServico { get; set; }
        public DateTime? DtAlt { get; set; }
        public Guid? VendaId { get; set; }
        public Guid? ClienteId { get; set; }
        public int? PerfilId { get; set; }
        public decimal? LimiteConsumo { get; set; }
        public DateTime? DtHrAbertura { get; set; }
        public string Apelido { get; set; }
        public string ApelidoOrigem { get; set; }
        public int? CodigoPai { get; set; }
        public bool? PermiteReserva { get; set; }
        public bool? Saiu { get; set; }
        public bool? PrePago { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
