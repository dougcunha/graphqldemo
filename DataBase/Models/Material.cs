﻿namespace GraphQLDemo.DataBase.Models
{
    using System;

    // ReSharper disable once PartialTypeWithSinglePart
    public partial class Material
    {
        public int Id { get; set; }
        public bool? Ativo { get; set; }
        public DateTime DtAlt { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string DescricaoTouch { get; set; }
        public string DescricaoProducao { get; set; }
        public string TeclaProg { get; set; }
        public string Imagem { get; set; }
        public string CodExterno { get; set; }
        public string Unidade { get; set; }
        public bool Venda { get; set; }
        public bool Servico { get; set; }
        public bool RequerObs { get; set; }
        public bool QtdeFrac { get; set; }
        public bool Balanca { get; set; }
        public bool Consumacao { get; set; }
        public bool VendeCombo { get; set; }
        public bool VendeWeb { get; set; }
        public int GrupoId { get; set; }
        public int? LocalProducaoId { get; set; }
        public long? LojaId { get; set; }
        public long RedeId { get; set; }
        public long? SubRedeId { get; set; }
        public decimal? CodigoNum { get; set; }
        public string DescricaoExtra { get; set; }
    }
}
