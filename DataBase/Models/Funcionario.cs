﻿namespace GraphQLDemo.DataBase.Models
{
    using System;

    // ReSharper disable once PartialTypeWithSinglePart
    public partial class Funcionario
    {
        public int Id { get; set; }
        public bool? Ativo { get; set; }
        public DateTime DtAlt { get; set; }
        public string Nome { get; set; }
        public short Codigo { get; set; }
        public string Senha { get; set; }
        public string Estado { get; set; }
        public string Usuario { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Cep { get; set; }
        public string Telefone { get; set; }
        public decimal? VlEntrega { get; set; }
        public int FuncaoId { get; set; }
        public int? GrupoId { get; set; }
        public int? UsuarioAppId { get; set; }
        public bool? AcessoSistema { get; set; }
        public bool? AdminMaster { get; set; }
    }
}
