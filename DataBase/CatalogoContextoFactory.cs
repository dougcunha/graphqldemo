﻿namespace GraphQLDemo.DataBase
{
    #region usings
    using System.IO;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;
    #endregion

    public class CatalogoContextoFactory : IDesignTimeDbContextFactory<CatalogoContexto>
    {
        /// <inheritdoc />
        public CatalogoContexto CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                               .SetBasePath(Directory.GetCurrentDirectory())
                               .AddJsonFile("appsettings.json")
                               .Build();

            var builder = new DbContextOptionsBuilder<CatalogoContexto>();
            var connectionString = configuration.GetConnectionString("ncrcolibri");
            builder.UseSqlServer(connectionString);

            return new CatalogoContexto(builder.Options);
        }
    }
}