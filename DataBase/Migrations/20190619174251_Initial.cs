﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
// ReSharper disable RedundantArgumentDefaultValue

namespace GraphQLDemo.DataBase.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "cliente",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false, defaultValueSql: "(newsequentialid())"),
                    identificacao = table.Column<string>(unicode: false, maxLength: 13, nullable: false),
                    tipo_id = table.Column<short>(nullable: true),
                    regiao_id = table.Column<int>(nullable: true),
                    midia_id = table.Column<short>(nullable: false),
                    nome = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    sexo = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    cnpj_cpf = table.Column<string>(unicode: false, maxLength: 18, nullable: true),
                    ie_rg = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    endereco = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    numero = table.Column<int>(nullable: true),
                    complemento = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    bairro = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    referencia = table.Column<string>(unicode: false, maxLength: 400, nullable: true),
                    cep = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    cidade = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    estado = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    telefone = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    fax = table.Column<string>(unicode: false, maxLength: 13, nullable: true),
                    foto = table.Column<byte[]>(type: "image", nullable: true),
                    desconto_id = table.Column<int>(nullable: true),
                    dt_validade_desconto = table.Column<DateTime>(type: "date", nullable: true),
                    dt_hr_primeira_compra = table.Column<DateTime>(type: "datetime", nullable: true),
                    dt_cadastro = table.Column<DateTime>(type: "date", nullable: true),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    conta = table.Column<bool>(nullable: false),
                    dt_validade_conta = table.Column<DateTime>(type: "date", nullable: true),
                    limite_conta = table.Column<double>(nullable: true, defaultValueSql: "((0))"),
                    dt_alt = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    id_externo = table.Column<int>(nullable: true),
                    lote_id = table.Column<long>(nullable: true, defaultValueSql: "((0))"),
                    status = table.Column<short>(nullable: true, defaultValueSql: "((0))"),
                    dt_nascimento = table.Column<DateTime>(type: "date", nullable: true),
                    dt_hr_ultima_compra = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cliente", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "funcionario",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    dt_alt = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    nome = table.Column<string>(maxLength: 40, nullable: false),
                    codigo = table.Column<short>(nullable: false),
                    senha = table.Column<string>(maxLength: 64, nullable: true),
                    estado = table.Column<string>(maxLength: 2, nullable: true),
                    usuario = table.Column<string>(maxLength: 10, nullable: true),
                    endereco = table.Column<string>(maxLength: 48, nullable: true),
                    bairro = table.Column<string>(maxLength: 25, nullable: true),
                    cidade = table.Column<string>(maxLength: 25, nullable: true),
                    cep = table.Column<string>(maxLength: 10, nullable: true),
                    telefone = table.Column<string>(maxLength: 15, nullable: true),
                    vl_entrega = table.Column<decimal>(type: "numeric(15, 2)", nullable: true),
                    funcao_id = table.Column<int>(nullable: false),
                    grupo_id = table.Column<int>(nullable: true),
                    usuario_app_id = table.Column<int>(nullable: true),
                    acesso_sistema = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    admin_master = table.Column<bool>(nullable: true, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_funcionario", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "material",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ativo = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    dt_alt = table.Column<DateTime>(type: "datetime", nullable: false),
                    codigo = table.Column<string>(maxLength: 20, nullable: false),
                    descricao = table.Column<string>(maxLength: 30, nullable: false),
                    descricao_touch = table.Column<string>(maxLength: 60, nullable: true),
                    descricao_producao = table.Column<string>(maxLength: 50, nullable: true),
                    tecla_prog = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    imagem = table.Column<string>(maxLength: 80, nullable: true),
                    cod_externo = table.Column<string>(maxLength: 30, nullable: true),
                    unidade = table.Column<string>(maxLength: 6, nullable: true),
                    venda = table.Column<bool>(nullable: false),
                    servico = table.Column<bool>(nullable: false),
                    requer_obs = table.Column<bool>(nullable: false),
                    qtde_frac = table.Column<bool>(nullable: false),
                    balanca = table.Column<bool>(nullable: false),
                    consumacao = table.Column<bool>(nullable: false),
                    vende_combo = table.Column<bool>(nullable: false),
                    vende_web = table.Column<bool>(nullable: false),
                    grupo_id = table.Column<int>(nullable: false),
                    local_producao_id = table.Column<int>(nullable: true),
                    loja_id = table.Column<long>(nullable: true),
                    rede_id = table.Column<long>(nullable: false),
                    sub_rede_id = table.Column<long>(nullable: true),
                    codigo_num = table.Column<decimal>(type: "decimal(20, 0)", nullable: true, computedColumnSql: "(CONVERT([decimal](20,0),[codigo],0))"),
                    descricao_extra = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_material", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ticket",
                columns: table => new
                {
                    ticket_id = table.Column<Guid>(nullable: false, defaultValueSql: "(newsequentialid())"),
                    ticket_pai_id = table.Column<Guid>(nullable: true),
                    modo_venda_id = table.Column<int>(nullable: false),
                    estado = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    codigo = table.Column<int>(nullable: false),
                    praca_id = table.Column<int>(nullable: true),
                    ativo = table.Column<bool>(nullable: false),
                    pct_servico = table.Column<double>(nullable: true),
                    dt_alt = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    venda_id = table.Column<Guid>(nullable: true),
                    cliente_id = table.Column<Guid>(nullable: true),
                    perfil_id = table.Column<int>(nullable: true),
                    limite_consumo = table.Column<decimal>(type: "money", nullable: true),
                    dt_hr_abertura = table.Column<DateTime>(type: "datetime", nullable: true),
                    apelido = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    apelido_origem = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    codigo_pai = table.Column<int>(nullable: true),
                    permite_reserva = table.Column<bool>(nullable: true),
                    saiu = table.Column<bool>(nullable: true),
                    pre_pago = table.Column<bool>(nullable: true, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ticket", x => x.ticket_id);
                    table.ForeignKey(
                        name: "ri_ticket$cliente_id__cliente$id",
                        column: x => x.cliente_id,
                        principalTable: "cliente",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_cliente$identificacao",
                table: "cliente",
                column: "identificacao");

            migrationBuilder.CreateIndex(
                name: "ix_cliente$nome",
                table: "cliente",
                column: "nome");

            migrationBuilder.CreateIndex(
                name: "ix_funcionario$codigo",
                table: "funcionario",
                column: "codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_funcionario$funcao_id",
                table: "funcionario",
                column: "funcao_id");

            migrationBuilder.CreateIndex(
                name: "ix_funcionario$grupo_id",
                table: "funcionario",
                column: "grupo_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$grupo_id",
                table: "material",
                column: "grupo_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$local_producao_id",
                table: "material",
                column: "local_producao_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$loja_id",
                table: "material",
                column: "loja_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$rede_id",
                table: "material",
                column: "rede_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$sub_rede_id",
                table: "material",
                column: "sub_rede_id");

            migrationBuilder.CreateIndex(
                name: "ix_material$ativo$venda",
                table: "material",
                columns: new[] { "ativo", "venda" });

            migrationBuilder.CreateIndex(
                name: "ix_material$rede_id$sub_rede_id$loja_id$codigo",
                table: "material",
                columns: new[] { "rede_id", "sub_rede_id", "loja_id", "codigo" },
                unique: true,
                filter: "[sub_rede_id] IS NOT NULL AND [loja_id] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ticket_cliente_id",
                table: "ticket",
                column: "cliente_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "funcionario");

            migrationBuilder.DropTable(
                name: "material");

            migrationBuilder.DropTable(
                name: "ticket");

            migrationBuilder.DropTable(
                name: "cliente");
        }
    }
}
