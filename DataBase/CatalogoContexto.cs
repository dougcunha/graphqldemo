﻿namespace GraphQLDemo.DataBase
{
    #region usings
    using GraphQLDemo.DataBase.Models;
    using Microsoft.EntityFrameworkCore;
    #endregion

    public class CatalogoContexto : DbContext
    {
        public CatalogoContexto() { }

        public CatalogoContexto(DbContextOptions<CatalogoContexto> options)
        : base(options) { }

        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Funcionario> Funcionario { get; set; }
        public virtual DbSet<Material> Material { get; set; }
        public virtual DbSet<Ticket> Ticket { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=localhost;Database=ncrcolibri;Trusted_Connection=True;");
            }
        }

        // ReSharper disable once FunctionComplexityOverflow
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Cliente>
            (
                entity =>
                    {
                        entity.ToTable("cliente");

                        entity.HasIndex(e => e.Identificacao)
                              .HasName("ix_cliente$identificacao");

                        entity.HasIndex(e => e.Nome)
                              .HasName("ix_cliente$nome");

                        entity.Property(e => e.Id)
                              .HasColumnName("id")
                              .HasDefaultValueSql("(newsequentialid())");

                        entity.Property(e => e.Ativo)
                              .IsRequired()
                              .HasColumnName("ativo")
                              .HasDefaultValueSql("((1))");

                        entity.Property(e => e.Bairro)
                              .HasColumnName("bairro")
                              .HasMaxLength(50)
                              .IsUnicode(false);

                        entity.Property(e => e.Cep)
                              .HasColumnName("cep")
                              .HasMaxLength(10)
                              .IsUnicode(false);

                        entity.Property(e => e.Cidade)
                              .HasColumnName("cidade")
                              .HasMaxLength(30)
                              .IsUnicode(false);

                        entity.Property(e => e.CnpjCpf)
                              .HasColumnName("cnpj_cpf")
                              .HasMaxLength(18)
                              .IsUnicode(false);

                        entity.Property(e => e.Complemento)
                              .HasColumnName("complemento")
                              .HasMaxLength(50)
                              .IsUnicode(false);

                        entity.Property(e => e.Conta)
                              .HasColumnName("conta");

                        entity.Property(e => e.DescontoId)
                              .HasColumnName("desconto_id");

                        entity.Property(e => e.DtAlt)
                              .HasColumnName("dt_alt")
                              .HasColumnType("datetime")
                              .HasDefaultValueSql("(getdate())");

                        entity.Property(e => e.DtCadastro)
                              .HasColumnName("dt_cadastro")
                              .HasColumnType("date");

                        entity.Property(e => e.DtHrPrimeiraCompra)
                              .HasColumnName("dt_hr_primeira_compra")
                              .HasColumnType("datetime");

                        entity.Property(e => e.DtHrUltimaCompra)
                              .HasColumnName("dt_hr_ultima_compra")
                              .HasColumnType("datetime");

                        entity.Property(e => e.DtNascimento)
                              .HasColumnName("dt_nascimento")
                              .HasColumnType("date");

                        entity.Property(e => e.DtValidadeConta)
                              .HasColumnName("dt_validade_conta")
                              .HasColumnType("date");

                        entity.Property(e => e.DtValidadeDesconto)
                              .HasColumnName("dt_validade_desconto")
                              .HasColumnType("date");

                        entity.Property(e => e.Email)
                              .HasColumnName("email")
                              .HasMaxLength(50)
                              .IsUnicode(false);

                        entity.Property(e => e.Endereco)
                              .HasColumnName("endereco")
                              .HasMaxLength(50)
                              .IsUnicode(false);

                        entity.Property(e => e.Estado)
                              .HasColumnName("estado")
                              .HasMaxLength(2)
                              .IsUnicode(false);

                        entity.Property(e => e.Fax)
                              .HasColumnName("fax")
                              .HasMaxLength(13)
                              .IsUnicode(false);

                        entity.Property(e => e.Foto)
                              .HasColumnName("foto")
                              .HasColumnType("image");

                        entity.Property(e => e.IdExterno)
                              .HasColumnName("id_externo");

                        entity.Property(e => e.Identificacao)
                              .IsRequired()
                              .HasColumnName("identificacao")
                              .HasMaxLength(13)
                              .IsUnicode(false);

                        entity.Property(e => e.IeRg)
                              .HasColumnName("ie_rg")
                              .HasMaxLength(15)
                              .IsUnicode(false);

                        entity.Property(e => e.LimiteConta)
                              .HasColumnName("limite_conta")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.LoteId)
                              .HasColumnName("lote_id")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.MidiaId)
                              .HasColumnName("midia_id");

                        entity.Property(e => e.Nome)
                              .HasColumnName("nome")
                              .HasMaxLength(50)
                              .IsUnicode(false);

                        entity.Property(e => e.Numero)
                              .HasColumnName("numero");

                        entity.Property(e => e.Referencia)
                              .HasColumnName("referencia")
                              .HasMaxLength(400)
                              .IsUnicode(false);

                        entity.Property(e => e.RegiaoId)
                              .HasColumnName("regiao_id");

                        entity.Property(e => e.Sexo)
                              .HasColumnName("sexo")
                              .HasMaxLength(1)
                              .IsUnicode(false);

                        entity.Property(e => e.Status)
                              .HasColumnName("status")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.Telefone)
                              .HasColumnName("telefone")
                              .HasMaxLength(20)
                              .IsUnicode(false);

                        entity.Property(e => e.TipoId)
                              .HasColumnName("tipo_id");
                    }
            );

            modelBuilder.Entity<Funcionario>
            (
                entity =>
                    {
                        entity.ToTable("funcionario");

                        entity.HasIndex(e => e.Codigo)
                              .HasName("ix_funcionario$codigo")
                              .IsUnique();

                        entity.HasIndex(e => e.FuncaoId)
                              .HasName("ix_funcionario$funcao_id");

                        entity.HasIndex(e => e.GrupoId)
                              .HasName("ix_funcionario$grupo_id");

                        entity.Property(e => e.Id)
                              .HasColumnName("id");

                        entity.Property(e => e.AcessoSistema)
                              .HasColumnName("acesso_sistema")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.AdminMaster)
                              .HasColumnName("admin_master")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.Ativo)
                              .IsRequired()
                              .HasColumnName("ativo")
                              .HasDefaultValueSql("((1))");

                        entity.Property(e => e.Bairro)
                              .HasColumnName("bairro")
                              .HasMaxLength(25);

                        entity.Property(e => e.Cep)
                              .HasColumnName("cep")
                              .HasMaxLength(10);

                        entity.Property(e => e.Cidade)
                              .HasColumnName("cidade")
                              .HasMaxLength(25);

                        entity.Property(e => e.Codigo)
                              .HasColumnName("codigo");

                        entity.Property(e => e.DtAlt)
                              .HasColumnName("dt_alt")
                              .HasColumnType("datetime")
                              .HasDefaultValueSql("(getdate())");

                        entity.Property(e => e.Endereco)
                              .HasColumnName("endereco")
                              .HasMaxLength(48);

                        entity.Property(e => e.Estado)
                              .HasColumnName("estado")
                              .HasMaxLength(2);

                        entity.Property(e => e.FuncaoId)
                              .HasColumnName("funcao_id");

                        entity.Property(e => e.GrupoId)
                              .HasColumnName("grupo_id");

                        entity.Property(e => e.Nome)
                              .IsRequired()
                              .HasColumnName("nome")
                              .HasMaxLength(40);

                        entity.Property(e => e.Senha)
                              .HasColumnName("senha")
                              .HasMaxLength(64);

                        entity.Property(e => e.Telefone)
                              .HasColumnName("telefone")
                              .HasMaxLength(15);

                        entity.Property(e => e.Usuario)
                              .HasColumnName("usuario")
                              .HasMaxLength(10);

                        entity.Property(e => e.UsuarioAppId)
                              .HasColumnName("usuario_app_id");

                        entity.Property(e => e.VlEntrega)
                              .HasColumnName("vl_entrega")
                              .HasColumnType("numeric(15, 2)");
                    }
            );

            modelBuilder.Entity<Material>
            (
                entity =>
                    {
                        entity.ToTable("material");

                        entity.HasIndex(e => e.GrupoId)
                              .HasName("ix_material$grupo_id");

                        entity.HasIndex(e => e.LocalProducaoId)
                              .HasName("ix_material$local_producao_id");

                        entity.HasIndex(e => e.LojaId)
                              .HasName("ix_material$loja_id");

                        entity.HasIndex(e => e.RedeId)
                              .HasName("ix_material$rede_id");

                        entity.HasIndex(e => e.SubRedeId)
                              .HasName("ix_material$sub_rede_id");

                        entity.HasIndex(e => new { e.Ativo, e.Venda })
                              .HasName("ix_material$ativo$venda");

                        entity.HasIndex(e => new { e.RedeId, e.SubRedeId, e.LojaId, e.Codigo })
                              .HasName("ix_material$rede_id$sub_rede_id$loja_id$codigo")
                              .IsUnique();

                        entity.Property(e => e.Id)
                              .HasColumnName("id");

                        entity.Property(e => e.Ativo)
                              .IsRequired()
                              .HasColumnName("ativo")
                              .HasDefaultValueSql("((1))");

                        entity.Property(e => e.Balanca)
                              .HasColumnName("balanca");

                        entity.Property(e => e.CodExterno)
                              .HasColumnName("cod_externo")
                              .HasMaxLength(30);

                        entity.Property(e => e.Codigo)
                              .IsRequired()
                              .HasColumnName("codigo")
                              .HasMaxLength(20);

                        entity.Property(e => e.CodigoNum)
                              .HasColumnName("codigo_num")
                              .HasColumnType("decimal(20, 0)")
                              .HasComputedColumnSql("(CONVERT([decimal](20,0),[codigo],0))");

                        entity.Property(e => e.Consumacao)
                              .HasColumnName("consumacao");

                        entity.Property(e => e.Descricao)
                              .IsRequired()
                              .HasColumnName("descricao")
                              .HasMaxLength(30);

                        entity.Property(e => e.DescricaoExtra)
                              .HasColumnName("descricao_extra")
                              .HasMaxLength(50);

                        entity.Property(e => e.DescricaoProducao)
                              .HasColumnName("descricao_producao")
                              .HasMaxLength(50);

                        entity.Property(e => e.DescricaoTouch)
                              .HasColumnName("descricao_touch")
                              .HasMaxLength(60);

                        entity.Property(e => e.DtAlt)
                              .HasColumnName("dt_alt")
                              .HasColumnType("datetime");

                        entity.Property(e => e.GrupoId)
                              .HasColumnName("grupo_id");

                        entity.Property(e => e.Imagem)
                              .HasColumnName("imagem")
                              .HasMaxLength(80);

                        entity.Property(e => e.LocalProducaoId)
                              .HasColumnName("local_producao_id");

                        entity.Property(e => e.LojaId)
                              .HasColumnName("loja_id");

                        entity.Property(e => e.QtdeFrac)
                              .HasColumnName("qtde_frac");

                        entity.Property(e => e.RedeId)
                              .HasColumnName("rede_id");

                        entity.Property(e => e.RequerObs)
                              .HasColumnName("requer_obs");

                        entity.Property(e => e.Servico)
                              .HasColumnName("servico");

                        entity.Property(e => e.SubRedeId)
                              .HasColumnName("sub_rede_id");

                        entity.Property(e => e.TeclaProg)
                              .HasColumnName("tecla_prog")
                              .HasMaxLength(1)
                              .IsUnicode(false);

                        entity.Property(e => e.Unidade)
                              .HasColumnName("unidade")
                              .HasMaxLength(6);

                        entity.Property(e => e.Venda)
                              .HasColumnName("venda");

                        entity.Property(e => e.VendeCombo)
                              .HasColumnName("vende_combo");

                        entity.Property(e => e.VendeWeb)
                              .HasColumnName("vende_web");
                    }
            );

            modelBuilder.Entity<Ticket>
            (
                entity =>
                    {
                        entity.ToTable("ticket");

                        entity.Property(e => e.TicketId)
                              .HasColumnName("ticket_id")
                              .HasDefaultValueSql("(newsequentialid())");

                        entity.Property(e => e.Apelido)
                              .HasColumnName("apelido")
                              .HasMaxLength(20)
                              .IsUnicode(false);

                        entity.Property(e => e.ApelidoOrigem)
                              .HasColumnName("apelido_origem")
                              .HasMaxLength(20)
                              .IsUnicode(false);

                        entity.Property(e => e.Ativo)
                              .HasColumnName("ativo");

                        entity.Property(e => e.ClienteId)
                              .HasColumnName("cliente_id");

                        entity.Property(e => e.Codigo)
                              .HasColumnName("codigo");

                        entity.Property(e => e.CodigoPai)
                              .HasColumnName("codigo_pai");

                        entity.Property(e => e.DtAlt)
                              .HasColumnName("dt_alt")
                              .HasColumnType("datetime")
                              .HasDefaultValueSql("(getdate())");

                        entity.Property(e => e.DtHrAbertura)
                              .HasColumnName("dt_hr_abertura")
                              .HasColumnType("datetime");

                        entity.Property(e => e.Estado)
                              .HasColumnName("estado")
                              .HasMaxLength(30)
                              .IsUnicode(false);

                        entity.Property(e => e.LimiteConsumo)
                              .HasColumnName("limite_consumo")
                              .HasColumnType("money");

                        entity.Property(e => e.ModoVendaId)
                              .HasColumnName("modo_venda_id");

                        entity.Property(e => e.PctServico)
                              .HasColumnName("pct_servico");

                        entity.Property(e => e.PerfilId)
                              .HasColumnName("perfil_id");

                        entity.Property(e => e.PermiteReserva)
                              .HasColumnName("permite_reserva");

                        entity.Property(e => e.PracaId)
                              .HasColumnName("praca_id");

                        entity.Property(e => e.PrePago)
                              .HasColumnName("pre_pago")
                              .HasDefaultValueSql("((0))");

                        entity.Property(e => e.Saiu)
                              .HasColumnName("saiu");

                        entity.Property(e => e.TicketPaiId)
                              .HasColumnName("ticket_pai_id");

                        entity.Property(e => e.VendaId)
                              .HasColumnName("venda_id");

                        entity.HasOne(d => d.Cliente)
                              .WithMany(p => p.Ticket)
                              .HasForeignKey(d => d.ClienteId)
                              .HasConstraintName("ri_ticket$cliente_id__cliente$id");
                    }
            );
        }
    }
}