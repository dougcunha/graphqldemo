﻿namespace GraphQLDemo
{
    #region usings
    using GraphQLDemo.DataAccess.Repositories.Contracts;
    using GraphQLDemo.DataBase;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using GraphiQl;
    using GraphQL;
    using GraphQL.Server.Ui.Playground;
    using GraphQLDemo.Queries;
    using GraphQL.Types;
    using GraphQLDemo.DataAccess.Repositories;
    using GraphQLDemo.Schema;
    using GraphQLDemo.Types;
    #endregion

    public class Startup
    {
        public Startup(IConfiguration configuration)
            => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseGraphiQl();
            app.UseGraphQLPlayground(options: new GraphQLPlaygroundOptions());
            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddTransient<IMaterialRepository, MaterialRepository>();
            services.AddTransient<IClienteRepository, ClienteRepository>();

            services.AddDbContext<CatalogoContexto>
            (
                options => options.UseSqlServer(Configuration["ConnectionStrings:ncrcolibri"])
            );

            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<CatalogoQuery>();
            services.AddSingleton<CatalogoMutation>();
            services.AddSingleton<MaterialType>();
            services.AddSingleton<MaterialInputType>();
            services.AddSingleton<ClienteType>();

            var sp = services.BuildServiceProvider();
            
            services.AddSingleton<ISchema>
            (
                new CatalogoSchema(new FuncDependencyResolver(type => sp.GetService(type)))
            );
        }
    }
}